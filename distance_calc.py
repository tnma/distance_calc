import sys

from PyQt5.QtWidgets import *
import controller


class Form(QWidget):
    def __init__(self, parent=None):
        super(Form, self).__init__(parent)
        self.from_label = QLabel("From:")
        self.from_line = QLineEdit()
        self.to_label = QLabel("To:")
        self.to_line = QLineEdit()
        self.calc_button = QPushButton("Calc")
        self.result_label = QLabel("")
        layout = QVBoxLayout()
        layout.addWidget(self.from_label)
        layout.addWidget(self.from_line)
        layout.addWidget(self.to_label)
        layout.addWidget(self.to_line)
        layout.addWidget(self.calc_button)
        layout.addWidget(self.result_label)
        self.calc_button.clicked.connect(self.submit_contact)
        main_layout = QGridLayout()
        main_layout.addLayout(layout, 0, 1)
        self.setLayout(main_layout)
        self.setWindowTitle("Distance Calculator")

    def submit_contact(self):
        result = controller.calc_distance(self.from_line.text(), self.to_line.text())
        # write result for two decimal places
        self.result_label.setText('Result: ' + result)


if __name__ == '__main__':
    app = QApplication(sys.argv)

    screen = Form()
    screen.show()

    sys.exit(app.exec_())
