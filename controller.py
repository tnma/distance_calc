import http.client
import json
import math

G_MAPS_API_KEY = ''
RAD_EARTH = 6371e3


def calc_distance(from_loc, to_loc):
    from_loc = from_loc.replace(' ', '+')
    to_loc = to_loc.replace(' ', '+')

    conn = http.client.HTTPSConnection('maps.googleapis.com')
    conn.request('GET', '/maps/api/geocode/json?address=' + from_loc + '&key=' + G_MAPS_API_KEY)
    r_from = conn.getresponse()
    from_res = json.loads(r_from.read().decode('utf-8'))
    if len(from_res['results']) < 1:
        return None
    from_coord = from_res['results'][0]['geometry']['location']
    print(from_coord)
    conn.close()

    conn = http.client.HTTPSConnection('maps.googleapis.com')
    conn.request('GET', '/maps/api/geocode/json?address=' + to_loc + '&key=' + G_MAPS_API_KEY)
    r_to = conn.getresponse()
    to_res = json.loads(r_to.read().decode('utf-8'))
    if len(to_res['results']) < 1:
        return None
    to_coord = to_res['results'][0]['geometry']['location']
    print(to_coord)
    conn.close()

    # return distance in miles
    return str('{0:.2f}'.format(distance_between_coordinates(from_coord, to_coord) * 0.000621371)) + ' miles'


def str_is_float(s):
    try:
        float(s)
        return True
    except ValueError:
        return False


def distance_between_coordinates(c1, c2):
    # haversine formula
    rad_lat1 = math.radians(c1['lat'])
    rad_lat2 = math.radians(c2['lat'])
    rad_lat_delta = math.radians(c2['lat'] - c1['lat'])
    rad_lng_delta = math.radians(c2['lng'] - c1['lng'])
    a = math.sin(rad_lat_delta / 2)**2 + math.cos(rad_lat1) * math.cos(rad_lat2) * math.sin(rad_lng_delta / 2)**2
    c = math.atan2(math.sqrt(a), math.sqrt(1 - a)) * 2
    # return distance in meters
    return RAD_EARTH * c
